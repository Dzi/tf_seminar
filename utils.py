import numpy as np
import matplotlib.pyplot as plt

NUM_CLASSES = 5


def onehot_converter(labels, num_classes=NUM_CLASSES):
    out = np.zeros((len(labels), num_classes))
    out[np.arange(len(labels)), labels] = 1
    return out


def data_reader(file_path='data/not_MNIST.npz'):
    data_handle = np.load(file_path)
    data = data_handle['arr_0']
    labels = data_handle['arr_1']
    return data, labels


def plot_accuracies(train_acc, test_acc):
    plt.plot(train_acc, 'b', label='Train Acc')
    plt.plot(test_acc, 'r', label='Test Acc')
    plt.legend()
    plt.show()


def plot_conf_mat(predicted_classes, test_labels):
    confusion_matrix = np.zeros([NUM_CLASSES, NUM_CLASSES])
    for p, t in zip(predicted_classes, test_labels):
        confusion_matrix[t, p] += 1
    plt.matshow(confusion_matrix)
    plt.colorbar()
    plt.show()
